O:\AUTO\Scripts\vbs\GENERIC\FilesEx.vbs Start Youcef



REM Download CDS FIPS Portfolio File

O:\AUTO\Scripts\vbs\BondSigma\Get_CDS_Port.vbs


REM Copy CDS FIPS Portfolio File to Archive

O:\AUTO\Scripts\vbs\FixedIncome\CopyFileOnly.vbs O:\Prodman\Dev\WFI\Feeds\CDS\FIPS\Port\ N:\No_Cull_Feeds\CDS_FIPS_Port\ .out


REM Rename CDS FIPS Portfolio File

O:\AUTO\Scripts\vbs\FixedIncome\Rename_FIPS.vbs O:\Prodman\Dev\WFI\Feeds\CDS\FIPS\Port\ .OUT O:\Prodman\Dev\WFI\Feeds\CDS\FIPS\Port\ CDS_FIPS.txt


REM Tidy CDS FIPS Portfolio File

powershell.exe O:\AUTO\Scripts\Powershell\File_Handler.ps1 -a tidy -s O:\Prodman\Dev\WFI\Feeds\CDS\FIPS\Port\ -t OUT -logfile CDS_FIPS_BondSigma


REM Database Load

O:\AUTO\Scripts\vbs\GENERIC\test\MySQL32.vbs Prices_DB O:\Prodman\Dev\WFI\Scripts\Mysql\CDS\FIPS\CDS_FIPS_DB_Loader.sql CDS_FIPS_BondSigma CDS_FIPS_BondSigma


REM Load Yield Full

REM O:\AUTO\Scripts\vbs\GENERIC\test\MySQL32.vbs Prices_DB O:\AUTO\Scripts\mysql\WFI\BondSigma\BondSigma_LoadYieldFull.sql Load_RAW_Yieldfull CDS_FIPS_BondSigma


REM Tidy CDS FIPS Portfolio File

powershell.exe O:\AUTO\Scripts\Powershell\File_Handler.ps1 -a tidy -s O:\Prodman\Dev\WFI\Feeds\CDS\FIPS\CalcIn\ -t zip -logfile CDS_FIPS_BondSigma


REM Generate CalcIn File

O:\AUTO\Scripts\vbs\GENERIC\Prod.vbs a Prices_DB O:\Prodman\Dev\WFI\Scripts\Mysql\CDS\FIPS\CDS_FIPS_Calcin.sql O:\Prodman\Dev\WFI\Feeds\CDS\FIPS\CalcIn\YYYY-MM-DD.csv CDS_FIPS_BondSigma_: CDS_FIPS_BondSigma


REM Zip CalcIn File

powershell.exe O:\AUTO\Scripts\Powershell\File_Handler.ps1 -a zip -s O:\Prodman\Dev\WFI\Feeds\CDS\FIPS\CalcIn\ -d CDS_FIPS_BondSigma_YYYY-MM-DD -t csv -logfile CDS_FIPS_BondSigma


REM Upload CDS FIPS CalcIn File

o:\auto\scripts\vbs\generic\ftps.vbs O:/Prodman/Dev/WFI/Feeds/CDS/FIPS/CalcIn/ /Bespoke/BondSigma/CDS_FIPS/CalcIn/YYYY-MM-DD.zip both Upload CDS_FIPS_BondSigma CDS_FIPS_BondSigma_: Youcef_FTP


