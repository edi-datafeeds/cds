O:\AUTO\Scripts\vbs\GENERIC\FilesEx.vbs Start Youcef



REM Tidy Feed zip

powershell.exe O:\AUTO\Scripts\Powershell\File_Handler.ps1 -a tidy -s O:\Prodman\Dev\WFI\Feeds\CDS\FIPS\Feed\ -t zip -logfile CDS_FIPS_BondSigma -i y

REM Tidy Feed txt

powershell.exe O:\AUTO\Scripts\Powershell\File_Handler.ps1 -a tidy -s O:\Prodman\Dev\WFI\Feeds\CDS\FIPS\Feed\ -t txt -logfile CDS_FIPS_BondSigma -i y



REM Get Calcout

O:\AUTO\Scripts\vbs\BondSigma\Get_CDS_CalcOut.vbs 


REM Archive CDS calcout

O:\AUTO\Scripts\vbs\FixedIncome\CopyFileOnly.vbs O:\Prodman\Dev\WFI\Feeds\CDS\FIPS\CalcOut\ N:\no_cull_feeds\CDS_Calcout\ .zip



REM Unzip CDS Calcout feed

O:\AUTO\Scripts\vbs\FixedIncome\Unzipper.vbs O:\Prodman\Dev\WFI\Feeds\CDS\FIPS\CalcOut\ CDS_FIPS_BondSigma_ YYYY-MM-DD O:\Prodman\Dev\WFI\Feeds\CDS\FIPS\CalcOut\

REM Load CDS Calcout

REM O:\AUTO\Scripts\vbs\BondSigma\CDS_CalcOut_Load.vbs Prices_DB


REM Rename CDS Calcout File

O:\AUTO\Scripts\vbs\FixedIncome\Rename_FIPS.vbs O:\Prodman\Dev\WFI\Feeds\CDS\FIPS\CalcOut\ .csv O:\Prodman\Dev\WFI\Feeds\CDS\FIPS\CalcOut\ CDS_Calcout.txt


REM Tidy CDS FIPS CalcOut zip File

powershell.exe O:\AUTO\Scripts\Powershell\File_Handler.ps1 -a tidy -s O:\Prodman\Dev\WFI\Feeds\CDS\FIPS\CalcOut\ -t zip -logfile CDS_FIPS_BondSigma


REM Database Load

O:\AUTO\Scripts\vbs\GENERIC\test\MySQL32.vbs Prices_DB O:\Prodman\Dev\WFI\Scripts\Mysql\CDS\FIPS\CDS_FIPS_CalcOut_DB_Loader.sql CDS_FIPS_BondSigma CDS_FIPS_BondSigma


REM Generate Yield Feed

O:\AUTO\Scripts\vbs\GENERIC\Prod.vbs a Prices_DB O:\AUTO\Scripts\mysql\WFI\BondSigma\CDS_Bondsigma_Feed.sql O:\Prodman\Dev\WFI\Feeds\CDS\FIPS\Feed\YYYY-MM-DD.txt :_CDS_TMX_BondSigma CDS_BondSigma_CalcOut


REM Rename CDS Yield Feed

O:\AUTO\Scripts\vbs\FixedIncome\Rename_FIPS.vbs O:\Prodman\Dev\WFI\Feeds\CDS\FIPS\Feed\ .txt O:\Prodman\Dev\WFI\Feeds\CDS\FIPS\Feed\ CDS_Yield.txt


REM Database Load Temp Yield Feed

O:\AUTO\Scripts\vbs\GENERIC\test\MySQL32.vbs Prices_DB O:\Prodman\Dev\WFI\Scripts\Mysql\CDS\FIPS\CDS_FIPS_Yield_DB_Loader.sql CDS_FIPS_BondSigma CDS_FIPS_BondSigma


REM Generate Temp Yield Feed

O:\AUTO\Scripts\vbs\GENERIC\Prod.vbs a Prices_DB O:\AUTO\Scripts\mysql\WFI\BondSigma\CDS_Bondsigma_Temp_Feed.sql O:\Prodman\Dev\WFI\Feeds\CDS\FIPS\Feed\YYYY-MM-DD.txt :_CDS_TMX_BondSigma CDS_BondSigma_CalcOut





REM Rename Lookup

O:\AUTO\Scripts\vbs\FixedIncome\Rename_FIPS.vbs O:\Datafeed\Debt\902\ .902 O:\Prodman\Dev\WFI\Feeds\CDS\FIPS\Feed\ LOOKUP.txt


REM Zip CDS Feed File

O:\AUTO\Scripts\vbs\FixedIncome\Zipper.vbs 102 none




REM FTP CDS Feed

o:\auto\scripts\vbs\generic\ftps.vbs O:/Prodman/Dev/WFI/Feeds/CDS/FIPS/Feed/ /Bespoke/CDS_TMX/Feed/YYYY-MM-DD.zip both Upload CDS_BondSigma_Yield :_CDS_TMX_BondSigma CDS_BondSigma_Yield
